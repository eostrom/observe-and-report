# observe-and-report
A simple script to check some network stuff for a list of hosts.

## Requirements
curl
ping
traceroute
date
tee

## Usage
``onr host1 [host2] [host3] ... [hostn]``

Output will be stored in ``./output``

